#include <queue.h>

Qret Q_push(QHdr Q, _QUEUE_ENTITY e) {
    QPoolEle qe = (Q->BASE + (Q->TAIL & Q->TC));
    Qret qret = (*qe != EOFQ) ? QUE(QFULL) : QPASS;
    if (qret == QPASS) {
        *qe = e;
        Q->CNT++;
        Q->TAIL++;
    }

    return qret;
}

_QUEUE_ENTITY Q_pop(QHdr Q) {
    QPoolEle qe = (Q->BASE + (Q->HEAD & Q->TC));
    if (*qe != EOFQ) {
        Q->HEAD++;
        Q->CNT--;
    }

    return *qe;
}

Qret Q_init(QHdr Q, uint16_t cnt) {
    //TODO cnt should be a power of 2

    if (Q_entityRest < cnt) {
        return QUE(QPOOL_EMPTY);}

    uint16_t i;
    QPoolEle qes = &Q_entityPool[Q_entityUsed];
    for (i = 0; i < cnt; i++) {
        *(qes + i) = (_QUEUE_ENTITY)INVALID; }
    Q_entityRest -= cnt;
    Q_entityUsed += cnt;
    Q->BASE = qes;
    Q->HEAD = 0;
    Q->TAIL = 0;
    Q->TC = cnt - 1;

    return QPASS;
}
