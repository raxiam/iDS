#ifndef ISOCKET_QUEUE_H
#define ISOCKET_QUEUE_H

#include <std/type.h>

#ifndef _QUEUE_ENTITY
#define _QUEUE_ENTITY PtrVale
#endif

typedef _QUEUE_ENTITY* QPoolEle;
#define EOFQ ((_QUEUE_ENTITY)INVALID) //end of queue
typedef struct QUEUE_DESC {
    QPoolEle    BASE;     //pointer to entity arrary
    uint16_t    TC;       //terminal count, +1 means size, must be power of 2
    uint16_t    CNT;      //element count, debug purpose only.
    uint16_t    HEAD;     //head index, pop from.
    uint16_t    TAIL;     //tail index, push to
} *QHdr, Queue;

typedef int16_t Qret;

//push entity e to Q
Qret Q_push(QHdr Q, _QUEUE_ENTITY e);
//pop an entity from Q if available, EOFQ will be returned if Q is empty
_QUEUE_ENTITY Q_pop(QHdr Q);
//alloc cnt element to Q, cnt must power of 2.
Qret Q_init(QHdr Q, uint16_t cnt);


#define QUEUE_MOD_ID 0x1100
#define QUE(ERR) -(QUEUE_MOD_ID | ERR)
typedef enum {
    QPASS = 0,
    QFULL,
    QPOOL_EMPTY,
    //TODO add xxx
    QSTS_MAX
} Qsts;


//_Q_ should be defined one & only--
#ifndef QELE_NUM
#define QELE_NUM 0x20
#endif

#ifndef _Q_
extern
#else
#pragma DATA_SECTION(Q_entityUsed, ".key.shared:Q")
#endif
uint16_t Q_entityUsed = 0;

#ifndef _Q_
extern
#else
#pragma DATA_SECTION(Q_entityRest, ".key.shared:Q")
#endif
uint16_t Q_entityRest = QELE_NUM;

#ifndef _Q_
extern
#else
#pragma DATA_SECTION(Q_elePool, ".key.shared:Q")
#endif
_QUEUE_ENTITY Q_entityPool[QELE_NUM];

#endif //ISOCKET_QUEUE_H
